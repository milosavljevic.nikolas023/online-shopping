<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<div class="container">

	<div class="row">
		<c:if test="${not empty message}">

			<div class="col-xs-12">

				<div class="alert alert-success alert-dismissible">

					<button type="button" class="close" data-dismiss="alert">&times;</button>

					${message}

				</div>

			</div>

		</c:if>



		<div class="col-md-offset-2 col-md-8">

			<div class="panel panel-primary">

				<div class="panel-heading">

					<h4>Upravljanje proizvodima</h4>

				</div>

				<div class="panel-body">

					<!-- FROM ELEMENTS -->
					<sf:form class="form-horisontal" modelAttribute="product"
						action="${contextRoot}/manage/products" method="POST"
						enctype="multipart/form-data">

						<div class="form-group">

							<label class="control-label col-md-4" for="name">Ime proizvoda:</label>

							<div class="col-md-8">

								<sf:input type="text" path="name" id="name"
									placeholder="Product Name" class="form-control" />
								<em class="help-block"></em>
								<sf:errors path="name" cssClass="help-block" element="em" />

							</div>

						</div>

						<div class="form-group">

							<label class="control-label col-md-4" for="brand">Ime brenda:</label>

							<div class="col-md-8">

								<sf:input type="text" path="brand" id="brand"
									placeholder="Brand Name" class="form-control" />
								<em class="help-block"></em>
								<sf:errors path="brand" cssClass="help-block" element="em" />
							</div>

						</div>

						<div class="form-group">

							<label class="control-label col-md-4" for="description">Opis proizvoda:</label>

							<div class="col-md-8">

								<sf:textarea path="description" id="description" rows="4"
									placeholder="Write a description" class="form-control" />
								<em class="help-block"></em>
								<sf:errors path="description" cssClass="help-block" element="em" />


							</div>

						</div>

						<div class="form-group">

							<label class="control-label col-md-4" for="price">Cena proizvoda:</label>

							<div class="col-md-8">

								<sf:input type="number" path="unitPrice" id="unitPrice"
									placeholder="Unit Price In Euros" class="form-control" />
								<sf:errors path="unitPrice" cssClass="help-block" element="em" />
								<em class="help-block"></em>

							</div>

						</div>

						<div class="form-group">

							<label class="control-label col-md-4" for="quantity">Kolicina proizvoda:</label>

							<div class="col-md-8">

								<sf:input type="number" path="quantity" id="quantity"
									placeholder="Quantity Avaliable" class="form-control" />
								<em class="help-block"></em>

							</div>

						</div>

						<!-- file element for image upload -->

						<div class="form-group">

							<label class="control-label col-md-4" for="file">Izaberite sliku:</label>

							<div class="col-md-8">

								<sf:input type="file" path="file" id="file" class="form-control" />
								<sf:errors path="file" cssClass="help-block" element="em" />
								<em class="help-block"></em>

							</div>

						</div>

						<div class="form-group">
							<label class="control-label col-md-4" for="categoryId">
								Select Category: </label>
							<div class="col-md-8">
								<sf:select class="form-control" id="categoryId"
									path="categoryId" items="${categories}" itemLabel="name"
									itemValue="id" />
								<div class="text-right">
									<c:if test="${product.id == 0}">

										<br />
										<button type="button" data-toggle="modal"
											data-target="#myCategoryModal" class="btn btn-warning btn-xs">Dodajte kategoriju</button>
									</c:if>
								</div>
								<em class="help-block"></em>

							</div>
						</div>


						<div class="form-group">

							<div class="col-md-offset-4 col-md-8">

								<input type="submit" name="submit" id="submit" value="Submit"
									class="btn btn-primary" />
								<!-- Hidden fields -->
								<sf:hidden path="id" />
								<sf:hidden path="code" />
								<sf:hidden path="supplierId" />
								<sf:hidden path="active" />
								<sf:hidden path="purchases" />
								<sf:hidden path="views" />

							</div>

						</div>

					</sf:form>

				</div>

			</div>

		</div>

	</div>

	<div class="row">

		<div class="col-xs-12">
			<h3>Dostupni proizvodi</h3>
		</div>

		<div class="col-xs-12">
			<div class="container-fluid">

				<div class="table-responsive">

					<!-- Products table for Admin -->
					<table id="adminProductsTable"
						class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Id</th>
								<th>&#160</th>
								<th>Ime</th>
								<th>Brend</th>
								<th>Kolicina</th>
								<th>Cena</th>
								<th>Stanje</th>
								<th>Uredi</th>
							</tr>

						</thead>



						<tfoot>
							<tr>
								<th>Id</th>
								<th>&#160</th>
								<th>Ime</th>
								<th>Brend</th>
								<th>Kolicina</th>
								<th>Cena</th>
								<th>Stanje</th>
								<th>Uredi</th>
							</tr>
						</tfoot>
					</table>

				</div>

			</div>



		</div>

	</div>

	<div class="modal fade" id="myCategoryModal" role="dialog"
		tabindex="-1">

		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<!-- Modal Header -->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span>&times;</span>
					</button>
					<h4 class="modal-title">Add New Category</h4>
				</div>
				<div class="modal-body">
					<!-- Category Form -->
					<sf:form id="categoryForm" class="form-horizontal"
						modelAttribute="category" action="${contextRoot}/manage/category"
						method="POST">

						<div class="form-group">
							<label class="control-label col-md-4">Name</label>
							<div class="col-md-8 validate">
								<sf:input type="text" path="name" class="form-control"
									placeholder="Category Name" />
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-4">Description</label>
							<div class="col-md-8 validate">
								<sf:textarea path="description" class="form-control"
									placeholder="Enter category description here!" />
							</div>
						</div>


						<div class="form-group">
							<div class="col-md-offset-4 col-md-4">
								<input type="submit" name="submit" value="Save"
									class="btn btn-primary" />
							</div>
						</div>
					</sf:form>
				</div>
			</div>
		</div>

	</div>

</div>



















