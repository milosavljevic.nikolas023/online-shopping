<div class="container">
	<div class="row">
		<!-- display sidebar -->

		<div class="col-md-3">

			<%@include file="./shared/sidebar.jsp"%>

		</div>

		<!-- display products -->

		<div class="col-md-9">
			<!-- added breadcrumb component -->
			<div class="row">
				<div class="col-lg-12">

					<c:if test="${userClickAllProducts == true}">

						<script>
							window.categoryId = '';
						</script>

						<ol class="breadcrumb">

							<li><a href="${contextRoot}/home">Pocetna stranica</a></li>
							<li class="active">Svi proizvodi</li>

						</ol>
					</c:if>

					<c:if test="${userClickCategoryProducts == true}">
						<script>
							window.categoryId = '${category.id}';
						</script>

						<ol class="breadcrumb">

							<li><a href="${contextRoot}/home}">Pocetna stranica</a></li>
							<li class="active">Category</li>
							<li class="active">${category.name}</li>

						</ol>
					</c:if>

				</div>
			</div>

			<div class="row">

				<div class="col-xs-12">

					<div class="container-fluid">

						<div class="table-responsive">

							<table id="productListTable"
								class="table table-striped table-borderd">

								<thead>

									<tr>
										<th></th>
										<th>Ime proizvoda</th>
										<th>Brend</th>
										<th>Cena proizvoda</th>
										<th>Dostupna kolicina</th>
										<th></th>
									</tr>

								</thead>
								<tfoot>

									<tr>
										<th></th>
										<th>Ime proizvoda</th>
										<th>Brend</th>
										<th>Cena proizvoda</th>
										<th>Dostupna kolicina</th>
										<th></th>

									</tr>

								</tfoot>

							</table>

						</div>

					</div>



				</div>

			</div>

		</div>
	</div>
</div>