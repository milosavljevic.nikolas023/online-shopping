
<div class="container">

	<div class="row">

		<div class="col-md-3">
			<%@include file="./shared/sidebar.jsp"%>
		</div>

		<div class="col-md-9">

			<div class="row carousel-holder">

				<div class="col-md-12">
					<div id="carousel-example-generic" class="carousel slide"
						data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#carousel-example-generic" data-slide-to="0"
								class="active"></li>
							<li data-target="#carousel-example-generic" data-slide-to="1"></li>
							<li data-target="#carousel-example-generic" data-slide-to="2"></li>
						</ol>
						<div class="carousel-inner">
							<div class="item active">
								<a href="show/11/product"> <img class="slide-image"
									src="/onlineshopping/resources/images/PRD5D48FEAF36.jpg" alt="">
								</a>
							</div>
							<div class="item">
								<a href="show/12/product"> <img class="slide-image"
									src="/onlineshopping/resources/images/PRD04592897E1.jpg" alt="">
								</a>
							</div>
							<div class="item">
								<a href="show/13/product"> <img class="slide-image"
									src="/onlineshopping/resources/images/PRDCC30707F5C.jpg" alt="">
								</a>
							</div>
						</div>
						<a class="left carousel-control" href="#carousel-example-generic"
							data-slide="prev"> <span
							class="glyphicon glyphicon-chevron-left"></span>
						</a> <a class="right carousel-control"
							href="#carousel-example-generic" data-slide="next"> <span
							class="glyphicon glyphicon-chevron-right"></span>
						</a>
					</div>
				</div>

			</div>

			<div class="row">

				<!-- =========================================ADDING THE PRODUCTS TO HOME PAGE====================================== -->

				<c:forEach items="${products}" var="product">

					<div class="col-sm-4 col-lg-4 col-md-4">
						<div class="thumbnail">
							<img id="homeImg" src="/onlineshopping/resources/images/${product.code}.jpg" alt=""> 
							<div class="caption">
								<h4 class="pull-right">${product.unitPrice} rsd</h4>
								<h4>
									<a href="#">${product.name}</a>
								</h4>
								<p>
									${product.description}
								</p>
							</div>
							
						</div>
					</div>
				</c:forEach>
				
				<!-- ======================================================================================================== -->

			</div>

		</div>

	</div>

</div>

<!-- /.container -->