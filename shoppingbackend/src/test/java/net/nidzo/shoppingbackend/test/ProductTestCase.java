package net.nidzo.shoppingbackend.test;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import net.nidzo.shoppingbackend.dao.ProductDAO;
import net.nidzo.shoppingbackend.dto.Product;

public class ProductTestCase {
	
	private static AnnotationConfigApplicationContext context;
	
	private static ProductDAO productDAO;
	
	private Product product;
	
	@BeforeClass
	public static void init() {
		context = new AnnotationConfigApplicationContext();
		context.scan("net.nidzo.shoppingbackend");
		context.refresh();
		productDAO = (ProductDAO)context.getBean("productDAO");
	}
	
//	@Test
//	public void testCRUDProduct() {
//		//create opperation
//		product = new Product();
//		
//		product.setName("Tesla 65S905");
//		product.setBrand("Tesla");
//		product.setDescription("This is best budget tv for the size");
//		product.setUnitPrice(25000);
//		product.setActive(true);
//		product.setCategoryId(2);
//		product.setSupplierId(3);
//		
//		assertEquals("Something went wrong while inserting a new product!", true, productDAO.add(product));
//	}
//		
//		//reading and updating the category
//		product = productDAO.get(2);
//		product.setName("Samsung Galaxy S7");
//		assertEquals("Something went wrong while updatiing the existing record!", true, productDAO.update(product));
//		
//		assertEquals("Something went wrong while deleting the existing recor!", true, productDAO.delete(product));
//		
//		//list
//		assertEquals("Something went wrong while listng products!", 7, productDAO.list().size());
//	}
	
//	@Test
//	public void testListActiveProducts() {
//		assertEquals("Something went wrong while listing active products!", 6, productDAO.listActiveProducts().size());
//	}
//	
//	@Test
//	public void testListActiveProductsByCategoty() {
//		assertEquals("Something went wrong while listing active products!", 4, productDAO.listActiveProductsByCategory(3).size());
//	}
//	@Test
//	public void testGetLastestActiveProduct() {
//		assertEquals("Something went wrong while listing active products!", 4, productDAO.getLatestActiveProducts(4).size());
//	}
}




















