package net.nidzo.shoppingbackend.test;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import net.nidzo.shoppingbackend.dao.UserDAO;
import net.nidzo.shoppingbackend.dto.Address;
import net.nidzo.shoppingbackend.dto.Cart;
import net.nidzo.shoppingbackend.dto.User;

public class UserTestCase {
	
	private static AnnotationConfigApplicationContext context;
	private static UserDAO userDAO;
	private User user = null;
	private Address address = null;
	private Cart cart = null;
	
	@BeforeClass
	public static void init() {
		context = new AnnotationConfigApplicationContext();
		context.scan("net.nidzo.shoppingbackend");
		context.refresh();
		
		userDAO = (UserDAO) context.getBean("userDAO");
	}
	
//	@Test
//	public void testAddress() {
//		
//		user = new User();
//		user.setFirstName("Hikaru");
//		user.setLastName("Nakamura");
//		user.setEmail("gmhikaru@gmail.com");
//		user.setContactNumber("12345678");
//		user.setRole("USER");
//		user.setPassword("123456");
//		
//		// add the user
//		assertEquals("Failed to add user!", true, userDAO.addUser(user));
//		
//		address = new Address();
//		address.setAddressLineOne("205/A Crazy Street, New Orleans");
//		address.setAddressLineTwo("Azerbaian");
//		address.setCity("Baku");
//		address.setState("Maharashtra");
//		address.setCountry("Serbia");
//		address.setPostalCode("4000001");
//		address.setBilling(true);
//		
//		//link the user with the address using user id
//		address.setUserId(user.getId());
//		
//		// add the user 
//		assertEquals("Failed to add address!", true, userDAO.addAddress(address));
//		
//		
//		if(user.getRole().contentEquals("USER")) {
//			
//			//create a cart for this user
//			cart = new Cart();
//			cart.setUser(user);
//			
//			//add the cart
//			assertEquals("Faild to add cart!", true, userDAO.addCart(cart));
//			
//			//add a shipping address for this user
//			address = new Address();
//			address.setAddressLineOne("205/A Crazy Street, New Orleans");
//			address.setAddressLineTwo("Azerbaian");
//			address.setCity("Baku");
//			address.setState("Maharashtra");
//			address.setCountry("Serbia");
//			address.setPostalCode("4000001");
//			address.setBilling(true);
//			
//			//set shipping to true
//			address.setShipping(true);
//			
//			//link it with the user
//			address.setUserId(user.getId());
//			
//			//add the cart
//			assertEquals("Failed to add cart!", true, userDAO.addAddress(address));
//			
//			
//		}
//	}
	
	
//	@Test
//	public void testAddress() {
//		user = new User();
//		user.setFirstName("Marko");
//		user.setLastName("Markovic");
//		user.setEmail("markovic@gmail.com");
//		user.setContactNumber("12345678");
//		user.setRole("USER");
//		user.setPassword("123456");
//		
//		if(user.getRole().contentEquals("USER")) {
//			
//			//create a cart for this user
//			cart = new Cart();
//			
//			cart.setUser(user);
//			
//			//attach cart with the user
//			user.setCart(cart);
//			
//		}
//		// add the user
//		assertEquals("Failed to add user!", true, userDAO.addUser(user));
//	}
	
//	@Test
//	public void testUpdateCart() {
//		
//		//fetch the user by email
//		user = userDAO.getByEmail("markovic@gmail.com");
//		
//		System.out.println("============================================User name is: " + user.getFirstName());
//		
//		//get the cart of the user
//		cart = user.getCart();
//		
//		System.out.println("============================================== CART: " + cart.getId());
//		
//		
//		cart.setGrandTotal(55552);
//		
//		cart.setCartLines(2);
//		
//		assertEquals("Failed to update the cart", true, userDAO.updateCart(cart));
//		
//	}
//	
//	@Test
//	public void testAddAddress() {
//		//adding an user
//		user = new User();
//		user.setFirstName("Nikola");
//		user.setLastName("Milosavljevic");
//		user.setEmail("nidzo@gmail.com");
//		user.setContactNumber("12345678");
//		user.setRole("USER");
//		user.setPassword("123456");
//		
//		// add the user
//		assertEquals("Failed to add user!", true, userDAO.addUser(user));
//		
//		//adding billing address
//		address = new Address();
//		address.setAddressLineOne("205/A Crazy Street, New Orleans");
//		address.setAddressLineTwo("Azerbaian");
//		address.setCity("Baku");
//		address.setState("Maharashtra");
//		address.setCountry("Serbia");
//		address.setPostalCode("4000001");
//		address.setBilling(true);
//		
//		//attached the user to the address
//		address.setUser(user);
//		
//		assertEquals("Failed to add address!", true, userDAO.addAddress(address));
//		
//		//shipping address
//		address = new Address();
//		address.setAddressLineOne("205/A Crazy Street, New Orleans");
//		address.setAddressLineTwo("Azerbaian");
//		address.setCity("Baku");
//		address.setState("Maharashtra");
//		address.setCountry("Serbia");
//		address.setPostalCode("4000001");
//		address.setShipping(true);
//		
//		//attached the user to the address
//		address.setUser(user);
//		
//		assertEquals("Failed to add shipping address!", true, userDAO.addAddress(address));
//	}
	
//	@Test
//	public void testAddress() {
//		
//		user = userDAO.getByEmail("nidzo@gmail.com");
//		
//		address = new Address();
//		address.setAddressLineOne("205/A Crazy Street, New Orleans");
//		address.setAddressLineTwo("Azerbaian");
//		address.setCity("Beograd");
//		address.setState("BALKAN BALKAN BALKAN");
//		address.setCountry("Serbia");
//		address.setPostalCode("4000001");
//		address.setShipping(true);
//		
//		//attached the user to the address
//		address.setUser(user);
//		
//		assertEquals("Failed to add shipping address!", true, userDAO.addAddress(address));
//	}
	
	@Test 
	public void testGetAddresses() {
		user = userDAO.getByEmail("nidzo@gmail.com");
		
		assertEquals("Failed to fetch the list of address and size does not match!", 2, userDAO.listShippingAddresses(user).size());
		
		assertEquals("Failed to fetch the list of address and size does not match!", "Baku", userDAO.getBillingAddress(user).getCity());
		
	}
}



































