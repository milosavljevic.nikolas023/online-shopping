package net.nidzo.shoppingbackend.test;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import net.nidzo.shoppingbackend.dao.CartLineDAO;
import net.nidzo.shoppingbackend.dao.ProductDAO;
import net.nidzo.shoppingbackend.dao.UserDAO;
import net.nidzo.shoppingbackend.dto.Cart;
import net.nidzo.shoppingbackend.dto.CartLine;
import net.nidzo.shoppingbackend.dto.Product;
import net.nidzo.shoppingbackend.dto.User;

public class CartLineTextCase {
	
	private static AnnotationConfigApplicationContext context;
	
	private static CartLineDAO cartLineDAO = null;
	private static ProductDAO productDAO = null;
	private static UserDAO userDAO = null;
	
	private Product product = null;
	private User user = null;
	private Cart cart = null;
	private CartLine cartLine = null;
	
	@BeforeClass
	public static void init() {
		context = new AnnotationConfigApplicationContext();
		context.scan("net.nidzo.shoppingbackend");
		context.refresh();
		productDAO = (ProductDAO)context.getBean("productDAO");
		userDAO = (UserDAO) context.getBean("userDAO");
		cartLineDAO = (CartLineDAO)context.getBean("cartLineDAO");
	}
	
	@Test
	public void testAddNewCartLine() {
		
		//get the user
		user = userDAO.getByEmail("vanja@gmail.com");
		
		//fetch the cart
		
		cart = user.getCart();
		
		//get product
		product = productDAO.get(8);
		
		//create new cartline
		cartLine = new CartLine();
		
		cartLine.setBuyingPrice(product.getUnitPrice());
		
		cartLine.setProductCount(1);
		
		cartLine.setTotal(cartLine.getProductCount() * product.getUnitPrice());
		
		cartLine.setAvailable(true);
		
		cartLine.setCartId(cart.getId());
		
		cartLine.setProduct(product);
		
		assertEquals("Failed to add the cartLine", true, cartLineDAO.add(cartLine));
		
		//update the cart
		System.out.println(cartLine.getTotal() + " ============================================================================");
		
		cart.setGrandTotal(cart.getGrandTotal() + cartLine.getTotal());
		cart.setCartLines(cart.getCartLines() + 1);
		
		assertEquals("Failed to ipdate the cart", true, cartLineDAO.updateCart(cart));
		
	}
	


}


















