package net.nidzo.shoppingbackend.test;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import net.nidzo.shoppingbackend.dao.CategoryDAO;
import net.nidzo.shoppingbackend.dto.Category;

public class CategoryTestCase {

	private static AnnotationConfigApplicationContext context;

	private static CategoryDAO categoryDAO;

	private Category category;

	@BeforeClass
	public static void init() {
		context = new AnnotationConfigApplicationContext();
		context.scan("net.nidzo.shoppingbackend");
		context.refresh();
		categoryDAO = (CategoryDAO) context.getBean("categoryDAO");
	}

//	@Test
//	public void testAddCategory() {
//		
//		category = new Category();
//		
//		category.setName("Laptop");
//		category.setDescription("This is some description for laptop!");
//		category.setImageURL("CAT_105.png");
//		
//		assertEquals("Successfully added a category inside the table!",true,categoryDAO.add(category));
//		
//		
//	}

//	@Test
//	public void testGetCategory() {
//		category = categoryDAO.get(1);
//		
//		assertEquals("Successfully fatched a single category","Laptop",category.getName());
//	}

	@Test
	public void testUpdateCategory() {

		category = categoryDAO.get(2);

		category.setActive(true);

		assertEquals("Successfully update a single category", true, categoryDAO.update(category));
	}
	
//	@Test
//	public void testDeleteCategory() {
//
//		category = categoryDAO.get(1);
//
//		assertEquals("Successfully deleted a single category", true, categoryDAO.delete(category));
//	}

	
//	@Test
//	public void testListCategory() {
//
//		assertEquals("Successfully fatched the list of caegories from the table!", 1, categoryDAO.list().size());
//	}
	
	
//	@Test
//	public void testCRUDCategory() {
//		//Add operation
//		category = new Category();
//		
//		category.setName("Laptop");
//		category.setDescription("This is some description for laptop!");
//		category.setImageURL("CAT_105.png");
//		
//		assertEquals("Successfully added a category inside the table!",true,categoryDAO.add(category));
//		
//		category = new Category();
//		
//		category.setName("Television");
//		category.setDescription("This is some description for television!");
//		category.setImageURL("CAT_105.png");
//		
//		assertEquals("Successfully added a category inside the table!",true,categoryDAO.add(category));
//		
//
//		
//		
//		//delete the category
//		
//	
//			category = categoryDAO.get(2);
//	
//			assertEquals("Successfully deleted a single category", true, categoryDAO.delete(category));
//
//			
//			//size of list
//			assertEquals("Successfully fatched the list of caegories from the table!", 4, categoryDAO.list().size());
//			
//	}
}














